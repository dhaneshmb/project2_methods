
// 1) key function :-

function keys(obj) {
    let keysArray = [];
    for(let key in obj) {
       keysArray.push(key);
    }
    return keysArray
}





// 2) value function :-

function values(obj) {
    let valueArray = [];
    for(let key in obj) {
        if(typeof(obj[key]) !== "function") {
           valueArray.push(obj[key]);
        }
    }
    return valueArray
}



// 3) mapObject function :-

function mapObject(obj, callBackFn) {
    if(callBackFn === undefined) {
        return "undefined is not a function"
    }
    let mapObj = {};
    for(let key in obj) {
        let value = obj[key];
        mapObj[key] = callBackFn(key, value);
    }
    return mapObj
}


// 4) pair function :-

function pairs(obj) {
    let objArray = [];
    for(let key in obj) {
        let value = obj[key];
        objArray.push([key, value])
    }
    return objArray
}




// 5) invert function :-


function invert(obj) {
  let invertObj = {};
  for(let key in obj) {
      let value = obj[key];
      invertObj[value] = key; 
  }
  return invertObj
}






// 6) default function :-

function defaults(obj, defaultProps) {
    if(obj === undefined && defaultProps === undefined) {
        return {}
    }
    for(let key in defaultProps) {
        if(!(key in obj)) {
            obj[key] = defaultProps[key];
        }
    }
    return obj;
}

module.exports = {
                  Keys : keys,
                  Values : values,
                  MapObject : mapObject,
                  Pairs : pairs,
                  Invert : invert,
                  Defaults : defaults
                 };



