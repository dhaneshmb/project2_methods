
// 1)

function counterFactory() {
    let counter = 5;
    function increment() {
        counter++;
        return counter
    }
    function decrement() {
        counter--;
        return counter
    }
    return {
            Increment : increment,
            Decrement : decrement
           }
}

let variableCounterFactory = counterFactory();




// 2)

function limitFunctionCallCount(cb, n) {
    let count = 0;
    function n_counter() {
        let argument = [...arguments];
        if(n > count) {
            count++;
            return cb(...argument)
        }else {
            return null
        }
    }
    return n_counter
}

let variableLimitFn = limitFunctionCallCount();



// 3)

function cacheFunction(cb) {
    let cache = {};
    function cbInvokeFunction() {
       let argument = [...arguments];
       let key = JSON.stringify(argument);
       if(key in cache) {
           return cache[key]
       }else {
           let value = cb(...argument);
           cache[key] = value;
           return value 
       }
    }
    return cbInvokeFunction
}

let variableCacheFn = cacheFunction();

module.exports = {
                 CounterFactory : counterFactory,
                 LimitFunctionCallCount : limitFunctionCallCount,
                 CacheFunction : cacheFunction
                 };