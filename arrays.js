

// 1) forEach function :-



function each(elements, callBackFn) {
    if(callBackFn === undefined) {
        console.log("undefined is not a function")
    }
    else if(elements === undefined) {
        console.log("elements is not defined")
    }
    else if(elements.length !== 0) {
        for(let index = 0; index < elements.length; index++) {
            let item = elements[index];
            if(item) {
                console.log(callBackFn(item, index, elements))
            }
        }
    }
}




// 2) map function :- 

function map(elements, callBackFn) {
    let mapArray = [];
    if(callBackFn === undefined) {
        return "undefined is not a function"
    }
    
    if(elements.length !== 0) {
        for(let index = 0; index < elements.length; index++) {
            let item = elements[index];
            mapArray.push(callBackFn(item, index, elements));
        }
    }
    return mapArray
}



// 3) reduce function :-
 

function reduce(elements, callBackFn, startingValue) {
    let accumulator;
    let start;
    if(callBackFn === undefined) {
        return "undefined is not a function"
    }
    if(startingValue === undefined) {
        accumulator = elements[0];
        start = 1;
    }else {
        accumulator = startingValue;
        start = 0;
    }
    if(elements.length !== 0) {
        for(let index = start; index < elements.length; index++) {
            let item = elements[index]; 
            accumulator = callBackFn(accumulator, item, index, elements);
        }
    }
    if(!accumulator){
       return "Reduce of empty array with no initial value"
    }
    return accumulator
}



// 4) find function :-


function find(elements, callBackFn) {
    if(callBackFn === undefined) {
        return "undefined is not a function"
    }
    for(let index = 0; index < elements.length; index++) {
        let item = elements[index];
        if(callBackFn(item, index, elements)) {
            return item;
        }
    }
}




// 5) filter function :-


function filter(elements, callBackFn) {
    let filterArray = [];
    if(callBackFn === undefined) {
        return "undefined is not a function"
    }
    if(elements.length !== 0){
        for(let index = 0; index < elements.length; index++) {
            let item = elements[index];
            if(callBackFn(item, index, elements)) {
                filterArray.push(item)
            }
        }
    }
    return filterArray;
}



// 6) flatten function :-

         
function flatten(elements) {
    let flatArray = [];
    if(elements === undefined) {
        return "elements is not defined"
    }
    if(elements) {
        for(let i = 0; i < elements.length; i++) {
            let item = elements[i];
            if(!Array.isArray(item)) {
                flatArray.push(item);
            } else {
                flatArray.push(...flatten(item));
            }
        }
    }
    return flatArray
}


module.exports = { Each : each,
                   Map  : map,
                   Reduce : reduce,
                   Find : find,
                   Filter : filter,
                   Flatten : flatten
};


  


